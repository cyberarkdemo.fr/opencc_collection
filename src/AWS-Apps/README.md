# AWS Apps - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to an AWS App or container, pre-installed on a Linux machine.

__Connection flow__:
```ascii
    RDP ┌───┐SSH ┌────────────┐RUN ┌────────────────────────┐
User───►│PSM├───►│Linux target├───►│App with AWS credentials│
        └───┘    └────────────┘    └────────────────────────┘
          ▲
          │Fetch SSH and AWS accounts
       ┌──┴──┐
       │Vault│
       └─────┘
```

Currently available connection components are:
 - `CC-AWS-CLI`: Run AWS CLI from Linux host
 - `CC-AWS-App-Container-CLI`: Run an container app with AWS credentials injected as environment variables.
 - `CC-Terraform-Container-CLI`: Run Terraform container with AWS credentials injected as environment variables.

## Prerequisites

Required prerequisites:

 - PVWA
 - Linux account (ssh key or password)

Connection Component specific prerequisites:
 - For `CC-AWS-CLI`:
    - aws command on Linux target ([documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html))
 - For `CC-AWS-App-Container-CLI` and `CC-Terraform-Container-CLI`:
     - Docker on Linux target ([documentation](https://docs.docker.com/engine/install/))

## Install

1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select AWS-Apps platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Application and platform AWS-Apps,
3. Select a safe to store the oc account in,
4. Define the properties as such:
    - AWS IAM Username: IAM username
    - Address: Linux target ip or hostname
    - Password & Confirm password: IAM access key secret
    - AWS Access Key ID
    - AWS Account ID Number
    - (optional) Container Engine: Choose between docker and podman. Required for containerized apps.
    - (optional) Container Image: Define container image to run when using `CC-AWS-App-Container-CLI`

## TODO / Enhancements

- [ ] PSMP Support
- [ ] CPM Plugin