# DBVisualizer - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a database via DBVisualizer.

__Connection flow__:
```ascii
    RDP ┌───┐RUN ┌────────────┐CONNECT ┌──────┐
User───►│PSM├───►│DBVisualizer├───────►│Target│
        └───┘    └────────────┘        └──────┘
          ▲
          │Fetch DB account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - PrivateArk client  
 - DBVisualizer (Free or Pro)
 - Database platform with `Policy ID`:
    - MSSql
    - Oracle
    - MySQL
    - MariaDB
    - PostgreSQL

## Install

1. Go to Administration > Platform Management, select the database platform and click on `Manage PSM connectors`,
2. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.
3. Run PrivateArk client and store PSM-DBVisualizerDispatcher.zip in `PSMUniversalConnectors` safe (replace the existing file). 

## Post Install
1. Install DBVisualizer (free or pro)
2. Run manually to accept EULA and select your version. The program should then generate configuration for JDBC drivers.
3. Copy `C:\Users\<USER WHO DID INSTALLATION>\.dbvis` to `C:\Program Files(x86)\CyberArk\PSM\Components\TEMPLATE.dbvis`
4. Go to `C:\Program Files (x86)\CyberArk\PSM\Components\TEMPLATE.dbvis\config70` and backup dbvis.xml
6. Edit dbvis.xml (the one from TEMPLATE.dbvis folder):
    - Remove the last 3 lines (should start with <Databases />)
    - Append the content of `C:\Program Files (x86)\CyberArk\PSM\Components\Connectors\PSM-DBVisualizerDispatcher\dbvis.xml`

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type `Database` and your database platform,
3. Select a safe to store the account in,
4. Fill in the account as such:
    - Username: DB username
    - Address: DB address
    - Password & Confirm password: DB password
    - Database: DB name
    - Port: DB port

## TODO / Enhancements

- [ ] Stop using PolicyID to select DB driver.
