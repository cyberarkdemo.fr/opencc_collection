#AutoIt3Wrapper_UseX64=n
Opt("MustDeclareVars", 0)
AutoItSetOption("WinTitleMatchMode", 1) ; Start of title is matched

;==========================================================================
;        PSM Universal Connector - DB Visualizer Dispatcher
;        ------------------------------------------------------------
;        Vendor:        DBVis
;        Product:       DB Visualizer
;==========================================================================

;Alters the method that is used to match window titles during search operations.
;1 = (default) Match the title from the start
;2 = Match any substring in the title
;3 = Exact title match
;4 = Advanced mode (retained for backwards compatibility only - see Window Titles & Text (Advanced))
;-1 to -4 = Case insensitive match according to the other type of match.

#include "../../PSMGenericClientWrapper.au3"
#include <FileConstants.au3>
#include <File.au3>
#include <Date.au3>
#include <Array.au3>
;#include <BlockInputEx.au3>    ; DEVELOPER - remove if not using blocking . Include in delivery if using it.
;#include <StringConstants.au3>


;=======================================
; Consts & Globals
;=======================================
Global Const $DISPATCHER_NAME                = "DbVisualizer"
Global $CLIENT_EXECUTABLE_PATH               = 'C:\Program Files\DbVisualizer\dbvis.exe'
Global $CLIENT_EXECUTABLE                    = "dbvis.exe"
Global Const $ERROR_MESSAGE_TITLE            = "PSM " & $DISPATCHER_NAME & " Dispatcher error message"
Global Const $LOG_MESSAGE_PREFIX             = $DISPATCHER_NAME & " Dispatcher - "

Global $TargetUsername
Global $TargetPassword
Global $TargetAddress

;Multi Database support
Global $TargetPolicyID
Global $TargetDriver
Global $TargetURLFormat                      = 0
Global $DriverOracle                         = "Oracle Thin"
Global $DriverSQLServer                      = "SQL Server (Microsoft JDBC Driver)"
Global $DriverMariaDB                        = "MariaDB"
Global $DriverMySQL                          = "MySQL"
Global $DriverPostgreSQL                     = "PostgreSQL"

Global $TargetPort
Global $TargetDatabase
Global $ProxyUser
Global $TargetRole
Global $ProfileDirectory                     = @UserProfileDir & '\.dbvis'
Global $ConnectionsDirectory                 = @UserProfileDir & '\.dbvis\config70'
Global $TemplateDirectory                    = 'C:\Program Files (x86)\CyberArk\PSM\Components\TEMPLATE.dbvis'
Global $ConnectionsFileName                  = 'dbvis.xml'

Global $ConnectionClientPID                  = 0
Global $sleep                                = 500
Global $longsleep                            = 5000

Global $TITLE                                = "DbVisualizer"
Global $TITLEVersions                        = "Available Versions"


;=======================================
; Code
;=======================================
Exit Main()

;=======================================
; Main
;=======================================
Func Main()

    ; Init PSM Dispatcher utils wrapper
    ToolTip ("Initializing...")
    if (PSMGenericClient_Init() <> $PSM_ERROR_SUCCESS) Then
        Error(PSMGenericClient_PSMGetLastErrorString())
    EndIf

    LogWrite("Successfully initialized Dispatcher Utils Wrapper")

    ; Get the dispatcher parameters
    FetchSessionProperties()

    LogWrite("Starting client application")
    ;ToolTip ("Starting " & $DISPATCHER_NAME & "...")

    ;_BlockInputEx(2) 
    PrepShadowProfile()

    $Cmd1 =  '-connection "' & $TargetDatabase & ' Database Connection" -userid ' & $TargetUsername & ' -password ' & $TargetPassword
    ShellExecute ($CLIENT_EXECUTABLE_PATH , $Cmd1)

    sleep($longsleep)

    Local $nCurrentSession = ProcessIdToSessionId(@AutoItPID)
    Local $ProcessList = ProcessList($CLIENT_EXECUTABLE)
    For $i = 1 To $ProcessList[0][0]
        If (ProcessIdToSessionId($ProcessList[$i][1]) == $nCurrentSession) Then
            $ConnectionClientPID = $ProcessList[$i][1]
            ExitLoop
        EndIf
    Next

    If ($ConnectionClientPID == 0) Then
        Error(StringFormat("Failed to execute process [%s]", $CLIENT_EXECUTABLE, @error))
    EndIf

    LoginProcess()

    ; Send PID to PSM as early as possible so recording/monitoring can begin
    LogWrite("Sending PID to PSM")
    if (PSMGenericClient_SendPID($ConnectionClientPID) <> $PSM_ERROR_SUCCESS) Then
        Error(PSMGenericClient_PSMGetLastErrorString())
    EndIf

    ; Terminate PSM Dispatcher utils wrapper
    LogWrite("Terminating Dispatcher Utils Wrapper")
    PSMGenericClient_Term()

    Return $PSM_ERROR_SUCCESS

EndFunc

;==================================
; Functions
;==================================
;DEVELOPER - relevant only if executable remaining differs from executable launched (for example splashscreen)

Func PrepshadowProfile()

    DirRemove($ProfileDirectory, 1)
    _TreeCopy($TemplateDirectory, $ProfileDirectory, 'none')
EndFunc

; #FUNCTION# ====================================================================================================================
; Name...........: ProcessIdToSessionId
; Description ...: Returns the session ID of the given process
; Parameters ....: $nProcessId - Error message to display
; Return values .: Session ID
; ===============================================================================================================================
Func ProcessIdToSessionId($nProcessId)

    Local $pSessionId = DllStructCreate("DWORD")
    Local $aResult = DllCall("kernel32.dll", "bool", "ProcessIdToSessionId", "DWORD", $nProcessId, "ptr", DllStructGetPtr($pSessionId))

    If @error Then
        Return SetError(@error, @extended, False)
    EndIf

    Return DllStructGetData($pSessionId, 1)
EndFunc


Func LoginProcess()

    LogWrite("Entered LoginProcess()")
    Sleep ($longsleep)

    While True
        Sleep ($sleep)
        Select
        Case WinExists($TITLEVersions)
            WinActivate($TITLEVersions)
            Send("{TAB}{SPACE}")
        Case WinActive($TITLE)
            ExitLoop
        EndSelect
    WEnd
EndFunc


; #FUNCTION# ====================================================================================================================
; Name...........: Error
; Description ...: An exception handler - Displays an error message and terminates the dispatcher
; Parameters ....: $ErrorMessage        - Error message to display
;                  $Code                - [Optional] Exit error code
; ===============================================================================================================================
Func Error($ErrorMessage, $Code = -1)

    ; If the dispatcher utils DLL was already initialized, write an error log message and terminate the wrapper
    if (PSMGenericClient_IsInitialized()) Then
        LogWrite($ErrorMessage, True)
        PSMGenericClient_Term()
    EndIf

    Local $MessageFlags = BitOr(0, 16, 262144) ; 0=OK button, 16=Stop-sign icon, 262144=MsgBox has top-most attribute set
    MsgBox($MessageFlags, $ERROR_MESSAGE_TITLE, $ErrorMessage)

    ; If the connection component was already invoked, terminate it
    if ($ConnectionClientPID <> 0) Then
        ProcessClose($ConnectionClientPID)
        $ConnectionClientPID = 0
    EndIf

    Exit $Code
EndFunc


; #FUNCTION# ====================================================================================================================
; Name...........: LogWrite
; Description ...: Write a log message to standard PSM log file
; Parameters ....: $sMessage          - [IN] The message to write
;                  $LogLevel          - [Optional] [IN] Defined if the message should be handled as an error message or as a trace message
; Return values .: $PSM_ERROR_SUCCESS - Success, otherwise error - Use PSMGenericClient_PSMGetLastErrorString for details.
; ===============================================================================================================================
Func LogWrite($sMessage, $LogLevel = $LOG_LEVEL_TRACE)

  Return PSMGenericClient_LogWrite($LOG_MESSAGE_PREFIX & $sMessage, $LogLevel)
EndFunc


; #FUNCTION# ====================================================================================================================
; Name...........: PSMGenericClient_GetSessionProperty
; Description ...: Fetches properties required for the session
; Parameters ....: None
; Return values .: None
; ===============================================================================================================================
Func FetchSessionProperties()

  Local $ErrorPrefix="Connector failed to retrieve value of attribute:"

    ;Multi Database support
    if (PSMGenericClient_GetSessionProperty("PolicyID", $TargetPolicyID) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Devicetype")
    EndIf
    ToolTip ("Loading Configuration for " & $TargetPolicyID)
    If ($TargetPolicyID == "Oracle") Then
        $TargetDriver = $DriverOracle
        $TargetURLFormat = 1
    ElseIf ($TargetPolicyID == "MSSql") Then
        $TargetDriver = $DriverSQLServer 
    ElseIf ($TargetPolicyID == "MySQL") Then
        $TargetDriver = $DriverSQLServer
    ElseIf ($TargetPolicyID == "MariaDB") Then
        $TargetDriver = $DriverMariaDB
    ElseIf ($TargetPolicyID == "PostgreSQL") Then
        $TargetDriver = $DriverPostgreSQL
    Else
        LogWrite ("Database Driver not configured for platform: " & $TargetPolicyID)
        Error($ErrorPrefix&"Database Driver not configured")
    EndIf

    ;Add controls to manage non mandatory attributes
    if (PSMGenericClient_GetSessionProperty("Address", $TargetAddress) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Address")
    EndIf

    if (PSMGenericClient_GetSessionProperty("Username", $TargetUsername) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Username")
    EndIf

    if (PSMGenericClient_GetSessionProperty("Password", $TargetPassword) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Password")
    EndIf

    if (PSMGenericClient_GetSessionProperty("Database", $TargetDatabase) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Database")
    EndIf

    if (PSMGenericClient_GetSessionProperty("Port", $TargetPort) <> $PSM_ERROR_SUCCESS) Then
        Error($ErrorPrefix&"Port")
    EndIf

EndFunc


Func _SingleFileCopy($SourceFile, $DestFile)

    If Not FileExists($SourceFile) Then
        ;error missing source
        Error('_SingleFileCopy: source file not found')
        Error(PSMGenericClient_PSMGetLastErrorString())
    Else
        LogWrite('_SingleFileCopy: Reading File - ' & $SourceFile & @CRLF)
        Dim $hFileOpen = FileOpen($SourceFile)
        ; Read the contents of the file using the handle returned by FileOpen.
        Dim $ThisFileData = FileRead($hFileOpen)
        ; Close the handle returned by FileOpen.
        FileClose($hFileOpen)
        LogWrite('_SingleFileCopy: Writing File - ' & $DestFile & @CRLF)
        FileDelete($DestFile)
        FileWrite($DestFile,$ThisFileData)
    EndIf
EndFunc


Func _TreeCopy($SourceDirPath, $DestDirPath, $ExistingFileExclusionList)

    Dim $EFList = StringSplit($ExistingFileExclusionList, ",")
    ;$SourceDirPath example = "C:\temp\sourceDir"
    ;$DestDirPath example = "C:\temp\destDir"
    ;Sanitize input ; trim the string in case a trailing \ was added to the end
    ;Optional:
    ;$ExistingFileExclusionList examples: The Full path of a source or a destination file you do not want to overwrite if the destination file path exists
    ;Logic Check: The Destination cannot be inside the source directory unless the destination is excluded - need to avoid infinite loop
    If (StringInStr($DestDirPath, $SourceDirPath) And Not StringInStr($ExistingFileExclusionList, $DestDirPath)) Then
        If $EFList[1] <> "none" Then
            If _ArraySearch($EFList, $DestDirPath) == -1 Then
                LogWrite ("_TreeCopy: An error occurred, The destination directory cannot be a Subdirectory of the Source path unless it is included in the ExistingFileExclusionList")
            EndIf
        Else
            LogWrite ("_TreeCopy: An error occurred, The destination directory cannot be a Subdirectory of the Source path unless it is included in the ExistingFileExclusionList")
        EndIf
    EndIf

    If (StringRight($SourceDirPath,1) == '\') Then $SourceDirPath = StringTrimRight( $SourceDirPath, 1)
    If (StringRight($DestDirPath,1) == '\') Then $DestDirPath = StringTrimRight($DestDirPath, 1)
    ;Get a list of all the directorys in the source
    Local $CmdText = '', $Pid = Run('"' & @ComSpec & '" /c ' & 'cmd.exe /k "dir /s/ad/b "' & $SourceDirPath & '"', $SourceDirPath, @SW_HIDE, 2 + 4)
    While 1
        $CmdText &= StdoutRead($Pid, False, False)
        If @error Then
            ExitLoop
        EndIf
        Sleep(10)
    WEnd
    Local $SourceDirList = StringReplace(StringStripWS($CmdText, 7),'>',"")
    Local $DestDirList = StringReplace($SourceDirList, $SourceDirPath, $DestDirPath)
    Local $DestDirArray = StringSplit($DestDirList, @CRLF)
    Local $SourceDirArray = StringSplit($SourceDirList, @CRLF)
    Dim $i
    For $i = 1 to (UBound($DestDirArray) ) -1
        $DoFileCopy = False
        If Not FileExists($DestDirArray[$i]) Then
            If _ArraySearch($EFList, $DestDirArray[$i]) <> -1 Then
                ;The target directory is in the exclude list
                LogWrite ('_TreeCopy: Skipping creation of excluded directory : ' & $DestDirArray[$i] & @CRLF)
                $DoFileCopy =False
            Else
                DirCreate($DestDirArray[$i])
                LogWrite("Creating Directory: " &$DestDirArray[$i] & @CRLF)
                $DoFileCopy = True
                if @error Then
                    LogWrite ('_TreeCopy: Error occurred while creating destination directory: ' & $DestDirArray[$i] & @CRLF)
                    Error(PSMGenericClient_PSMGetLastErrorString())
                    ;$DoFileCopy =False
                    Exit
                EndIf
            EndIf
        ElseIf _ArraySearch($EFList, $DestDirArray[$i]) <> -1 Then
            ;The target directory is in the exclude list
            LogWrite ('_TreeCopy: Skipping creation of excluded directory : ' & $DestDirArray[$i] & @CRLF)
            $DoFileCopy =False
        Else
            LogWrite ('_TreeCopy: Directory already Exists: ' & $DestDirArray[$i] & @CRLF)
            $DoFileCopy = True
        EndIf
        ;_ArrayDisplay($SourceDirArray)

        If $DoFileCopy == True Then
            ;Local $ThisFileArray = _FileListToArray($SourceDirArray[$i],"*",$FLTA_FILES)
            Local $ThisFileArray = _FileListToArray($SourceDirArray[$i],"*",1)
            ;_ArrayDisplay($ThisFileArray)
            Dim $z
            For $z = 1 to UBound($ThisFileArray) -1
                LogWrite('_TreeCopy: Reading File - ' & $SourceDirArray[$i]&"\"&$ThisFileArray[$z] & @CRLF)
                Dim $hFileOpen = FileOpen($SourceDirArray[$i]&"\" & $ThisFileArray[$z])
                ; Read the contents of the file using the handle returned by FileOpen.
                Dim $ThisFileData = FileRead($hFileOpen)
                ; Close the handle returned by FileOpen.
                FileClose($hFileOpen)
                If ($ThisFileArray[$z] == $ConnectionsFileName) Then
                    LogWrite('_TreeCopy: Updating Hod File' & @CRLF)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetDriver~", $TargetDriver)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetPort~", $TargetPort)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetAddress~", $TargetAddress)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetDatabase~", $TargetDatabase)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetUsername~", $TargetUsername)
                    $ThisFileData = StringReplace($ThisFileData, "~TargetURLFormat~", $TargetURLFormat)
                EndIf
                If _ArraySearch($EFList, $DestDirArray[$i]&"\"&$ThisFileArray[$z]) == -1 Then
                    ; The destination file is not excluded.
                    LogWrite ("_TreeCopy: Writing File- " & $DestDirArray[$i] & "\" & $ThisFileArray[$z] & @CRLF)
                    FileDelete($DestDirArray[$i] &"\" & $ThisFileArray[$z])
                    FileWrite($DestDirArray[$i]&"\" & $ThisFileArray[$z],$ThisFileData)
                Else
                    LogWrite ("_TreeCopy: Skipping Excluded File - " & $DestDirArray[$i] & "\" & $ThisFileArray[$z] & @CRLF)
                EndIf
            Next
        Else
            LogWrite('Skipping file copy on excluded directory:' &  $DestDirArray[$i] & @CRLF)
        EndIf
    Next
EndFunc

