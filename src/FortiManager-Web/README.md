# FortiManager Web - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a FortiManager Web portal.

__Connection flow__:
```ascii
    RDP ┌───┐HTTP┌────────────┐
User───►│PSM├───►│FortiManager│
        └───┘    └────────────┘
          ▲
          │Fetch Fortinet account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - FortiManager account
 - Chrome installed on PSM
   - Expected installation folder is: `C:\Program Files (x86)\Google\Chrome\`
   - PSM Applocker configuration must allow Chrome to be executed.

## Install

1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select FortiManager-Web platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Website and platform FortiManager-Web,
3. Select a safe to store the account in,
4. Define the properties as such:
    - Username: FortiManager login
    - Address: FortiManager address
    - Password & Confirm password: FortiManager password

## TODO / Enhancements

- [ ] CPM Plugin
