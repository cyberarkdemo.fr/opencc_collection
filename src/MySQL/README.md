# MySQL - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a database via MySQL Workbench.

__Connection flow__:
```ascii
    RDP ┌───┐RUN ┌───────────────┐CONNECT ┌──────┐
User───►│PSM├───►│MySQL Workbench├───────►│Target│
        └───┘    └───────────────┘        └──────┘
          ▲
          │Fetch DB account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - PrivateArk client
 - MySQL Workbench 8.0 installed on PSM
 - MySQL platform from Marketplace

## Install

1. Go to Administration > Platform Management, select the database platform and click on `Manage PSM connectors`,
2. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.
3. Run PrivateArk client and store PSM-MySQLWorkbench.zip in `PSMUniversalConnectors` safe (replace the existing file).

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type `Database` and your database platform,
3. Select a safe to store the account in,
4. Fill in the account as such:
    - Username: DB username
    - Address: DB address
    - Password & Confirm password: DB password

## TODO / Enhancements
