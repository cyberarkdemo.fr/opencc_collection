# Teamviewer - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a target via Teamviewer.

__Connection flow__:
```ascii
    RDP ┌───┐RUN ┌──────────┐CONNECT ┌──────┐
User───►│PSM├───►│Teamviewer├───────►│Target│
        └───┘    └──────────┘        └──────┘
          ▲
          │Fetch Teamviewer account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - Teamviewer account

## Install

1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select Teamviewer platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Application and platform Teamviewer,
3. Select a safe to store the account in,
4. Define the properties as such:
    - Username: Teamviewer ID
    - Address: unused, you can set the target computer address for example
    - Password & Confirm password: Teamviewer password

## TODO / Enhancements

- [ ] CPM Plugin