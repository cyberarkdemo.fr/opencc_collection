# Openshift CLI - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to an Openshift CLI (oc command), pre-installed on a Linux machine.

__Connection flow__:
```ascii
    RDP ┌───┐SSH ┌────────────┐HTTP┌──────────┐
User───►│PSM├───►│Linux target├───►│OC Cluster│
        └───┘    └────────────┘    └──────────┘
          ▲
          │Fetch SSH and OC accounts
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - Linux account (ssh key or password)
 - oc command installed ([documentation](https://docs.openshift.com/container-platform/4.3/cli_reference/openshift_cli/getting-started-cli.html#installing-the-cli))

## Install

1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select Openshift-CLI platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Application and platform Openshift-CLI,
3. Select a safe to store the oc account in,
4. Define the properties as such:
    - Username: Openshift login
    - Address: Linux target ip or hostname
    - Password & Confirm password: Openshift password
    - Openshift Address: Openshift cluster address (https://<>:port)

## TODO / Enhancements

- [ ] PSMP Support
- [ ] Containerized CC
- [ ] CPM Plugin

 