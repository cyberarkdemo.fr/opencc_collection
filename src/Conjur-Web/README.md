# Openshift CLI - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a Conjur web interface.

__Connection flow__:
```ascii
    RDP ┌───┐HTTPS┌───────────────┐
User───►│PSM├────►│Conjur Web site│
        └───┘     └───────────────┘
          ▲
          │Fetch Conjur Account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - Conjur Account

## Install
1. Go to the market place and download Conjur Users Plugin platform ([marketplace](https://cyberark-customers.force.com/mplace/s/#a352J000000aZvkQAE-a392J000000fB8yQAE))
1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select Conjur Users via REST  platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.

## Configure

1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Imported Platforms and platform Conjur Users via REST,
3. Select a safe to store the Conjur account in,
4. Define the properties as such:
    - Username: Conjur Username
    - Address: Conjur leader loadbalancer
    - Password & Confirm password: Conjur password
    - AWSAccessKeyID: NA

