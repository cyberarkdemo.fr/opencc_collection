# Openshift CLI - OpenCC Collection

## How it works

This feature allows the PSM to automatically connect to a Conjur Cli.

__Connection flow__:
```ascii
    RDP ┌───┐SSH ┌────────────┐HTTPS┌─────────────┐
User───►│PSM├───►│Linux target├────►│Conjur Leader│
        └───┘    └────────────┘     └─────────────┘
          ▲
          │Fetch Conjur Account
       ┌──┴──┐
       │Vault│
       └─────┘
```

## Prerequisites

 - PVWA
 - Linux account (ssh key or password)
 - Conjur Account
 - Conjur Cli installed (docker pull cyberark/conjur-cli)

## Install
1. Go to the market place and download Conjur Users Plugin platform ([marketplace](https://cyberark-customers.force.com/mplace/s/#a352J000000aZvkQAE-a392J000000fB8yQAE))
1. Go to Administration > Platform Management and import platform zip file using `Import platform` button,
2. Select Conjur Users via REST  platform and click on `Manage PSM connectors`,
3. Import connection component zip by Drag & Drop or by clicking on `Connector Package`.
4. Edit the platform to add the logon account capabilitie (UI & Worflows > Linked Accounts > Add Link [Name : LogonAccount, PropertyIndex : 1])
5. Edit the logon sequence to set the correct conjur name (options > Connection Components > PSM-CLI-User > Target Settings > Client Specific > AutoLogonSequenceWithLogonAccount [change the value cybr by the one that fit your environment])

## Configure


1. Go to Accounts > Accounts View and click on `Add account`,
2. Select System type Imported Platforms and platform Conjur Users via REST
3. Select a safe to store the Conjur account in,
4. Define the properties as such:
    - Logon Account : The linux account that will launch the cli
    - Username: Conjur Username
    - Address: Conjur leader loadbalancer
    - Password & Confirm password: Conjur password
    - AWSAccessKeyID: NA

