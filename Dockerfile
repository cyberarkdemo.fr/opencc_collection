FROM python:alpine

ENV USER=docker
ENV APP_UID=1000

WORKDIR /app

# Set up our runtime user.
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "$(pwd)" \
    --no-create-home \
    --uid "$APP_UID" \
    "$USER"

COPY . .

RUN chown -R "$APP_UID:$APP_UID" "$PWD"

USER "$USER"

