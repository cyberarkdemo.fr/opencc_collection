# OpenCC Collection

An open collection of connection components and platforms.

## Available plugins

Please look at the **[release page](https://gitlab.com/cyberarkdemo.fr/opencc_collection/-/releases)** for **ready to use** plugins.  

The plugin documentation is available as a README.md in `src/`, in the folder of the same name  
Eg: **CC-Openshift-CLI** and **PL-Openshift-CLI** documentation is in [`src/Openshift-CLI/README.md`](https://gitlab.com/cyberarkdemo.fr/opencc_collection/-/blob/master/src/Openshift-CLI/README.md).

Alternatively you generate the same plugins by cloning this project and running the following command:
```bash
$ docker run --pull always -it --mount "type=bind,src=$PWD/src,dst=/app/src" --mount "type=bind,src=$PWD/dist,dst=/app/dist" registry.gitlab.com/cyberarkdemo.fr/opencc_collection python package.py
```
It will generate plugins in the `dist/` folder.

## Contributing

Contributions to this project are welcome. First, you must:
1. Fork the project
2. Clone it locally
3. Create a branch (eg: feature/my-plugin)

### Add a plugin

You can add a plugin by respecting the below folder structure, to allow the generation of an archive:
 
```bash
.
├── src
│   ├── <my-feature>                                    # A feature is defined by a platform and several connection components
│   │   │
│   │   ├── CC-<my-connection-component-name>           # Connection component declaration
│   │   │   └── CC-<my-connection-component-name>.xml   # Connection XML configuration
│   │   │
│   │   ├── PL-<my-feature>                             # Platform definition
│   │   │   ├── Policy-<my-feature>.ini                 # Platform INI configuration
│   │   │   └── Policy-<my-feature>.xml                 # Platform XML configuration
│   │   │
│   │   └── README.md                                   # Instructions for this feature
```

**NB:** Use [openCC](https://gitlab.com/cyberarkdemo.fr/opencc) to easily extract connection components from a PVWA instance.  

### Build plugins

Once you added a plugin definition, you build it using the following commands:
```bash
$ docker build -t opencc_collection .
$ docker run -it --mount "type=bind,src=$PWD/src,dst=/app/src" --mount "type=bind,src=$PWD/dist,dst=/app/dist" opencc_collection python package.py
```

Once done, generated plugins will be available in the `dist` folder. You can now:
1. Validate your plugins
    - Import platform
    - Import connection components
    - Add account to platform
    - Test connection components
2. Submit a merge request. Once approved, your plugin will be added to the next release.

## Licensing

Copyright © 2021 CyberArk Software Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
