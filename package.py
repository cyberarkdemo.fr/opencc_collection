#!/usr/bin/env python
"""cyberarkdemo.fr - Plugin Builder"""

import os
import logging
import tempfile
import shutil


def set_logging(level=logging.INFO):
    """Set logging level"""
    new_logger = logging.getLogger()
    new_logger.setLevel(level)
    ch = logging.StreamHandler()
    new_logger.addHandler(ch)

    return new_logger


def create_temporary_folder(path):
    """Create a temporary folder named path"""
    temp_root = tempfile.mkdtemp()
    temp_path = os.path.join(temp_root, os.path.basename(os.path.normpath(path)))
    os.makedirs(temp_path)

    return temp_path


def create_temporary_copy(src, copy_name=None):
    """Create a copy of src in a temporary folder based on dst name"""
    if not copy_name:
        copy_name = src

    temp_root = tempfile.mkdtemp()
    temp_path = os.path.join(temp_root, os.path.basename(os.path.normpath(copy_name)))
    return shutil.copytree(src, temp_path)


def zip_plugin(src, dst):
    """Zip src folder and save in dst"""
    plugin_name = os.path.basename(os.path.normpath(src))
    root_dir = os.path.abspath(src)

    os.makedirs(dst, exist_ok=True)
    owd = os.getcwd()
    os.chdir(dst)
    shutil.make_archive(plugin_name, 'zip', root_dir)
    os.chdir(owd)


def export_plugin(feature, src, dst):
    feature_dst = os.path.join(dst, feature)
    os.makedirs(feature_dst, exist_ok=True)
    zip_plugin(src, feature_dst)


if __name__ == "__main__":
    logger = set_logging()
    input_dir = 'src'
    output_dir = 'dist'

    # Iterating over each folder
    for feature_dir_name in os.listdir(input_dir):

        feature_dir = os.path.join(input_dir, feature_dir_name)
        # Ignore some files and folders based on name
        if not os.path.isdir(feature_dir):
            logger.debug("Ignore {}".format(feature_dir_name))
            continue
        if feature_dir_name.startswith('.'):
            logger.debug("Ignore {}".format(feature_dir_name))
            continue

        logger.info("Process feature {}".format(feature_dir_name))
        for plugin_dir_name in os.listdir(feature_dir):

            plugin_dir = os.path.join(feature_dir, plugin_dir_name)
            # Ignore files in the feature dir, (eg: README.md)
            if not os.path.isdir(plugin_dir):
                logger.debug("Ignore {}".format(plugin_dir_name))
                continue

            # Search for Connection Components folders
            if plugin_dir_name.startswith("CC-"):
                logger.info("Found connection component {}".format(plugin_dir_name))

                # If the connection component is only the xml file, create the zip
                plugin_files = os.listdir(plugin_dir)
                if len(plugin_files) == 1 and plugin_files[0].endswith('.xml'):
                    export_plugin(feature_dir_name, plugin_dir, output_dir)
                else:
                    # Create 2 copies:
                    #   - PSM-name-of-plugin that contains all other files and executables.
                    #   - CC-name-of-plugin that contains only the xml config
                    # Mandatory because the connector upload api only accept a zip with xml file.
                    # The PSM package must be uploaded to the PSMUniversalConnector safe.
                    psm_package_dir_name = plugin_dir.replace("CC", "PSM", 1)
                    psm_package_tmp = create_temporary_copy(plugin_dir, psm_package_dir_name)

                    cc_package_tmp = create_temporary_folder(plugin_dir)
                    shutil.move(os.path.join(psm_package_tmp, "{}.xml".format(plugin_dir_name)),
                                cc_package_tmp)

                    export_plugin(feature_dir_name, psm_package_tmp, output_dir)
                    export_plugin(feature_dir_name, cc_package_tmp, output_dir)

            # Search for platforms
            elif plugin_dir_name.startswith("PL-"):
                logger.info("Found platform {}".format(plugin_dir_name))
                platform_dir_tmp = create_temporary_copy(plugin_dir)
                export_plugin(feature_dir_name, platform_dir_tmp, output_dir)
